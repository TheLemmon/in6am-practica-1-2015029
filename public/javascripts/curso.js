var myViewModel = {
    descripcion: ko.observable(),
    grupos: ko.observableArray(),

    ajaxHelper: function(uri, method, data){
        return $.ajax({
                url : uri,
                type: method,
                dataType: 'json',
                contentType: 'application/json',
                data: data ? JSON.stringify(data) : null
            }).fail(function(jqXHR, textStatus, errorThrown){
                console.log(errorThrown);
            })
    },

    mostrar: function(){
        myViewModel.ajaxHelper('/api/grupo', 'GET').done(function(data) {
            myViewModel.grupos(data)
        });
    },

    agregar: function(formElement){
        var data = {
            descripcion: myViewModel.descripcion()
        }
        myViewModel.ajaxHelper('/api/grupo', 'POST', data).done(function(data) {
            myViewModel.mostrar()
            $('#agregarCurso').modal('hide')
        });
    },
}

$(document).ready(function(){
    
    myViewModel.mostrar()
})