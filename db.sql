CREATE DATABASE Horario;
USE Horario;

CREATE TABLE Grupo(
	idGrupo INT AUTO_INCREMENT NOT NULL,
    descripcion VARCHAR(30) NOT NULL,
    PRIMARY KEY(idGrupo)
);

CREATE TABLE Curso(
	idCurso INT AUTO_INCREMENT NOT NULL,
    descripcion VARCHAR(50) NOT NULL,
    PRIMARY KEY(idCurso)
);

CREATE TABLE Asignacion(
	idAsignacion INT AUTO_INCREMENT NOT NULL,
    idGrupo INT NOT NULL,
    idCurso INT NOT NULL,
    FOREIGN KEY(idGrupo) REFERENCES Grupo(idGrupo) ON DELETE CASCADE,
    FOREIGN KEY(idCurso) REFERENCES Curso(idCurso) ON DELETE CASCADE,
    PRIMARY KEY(idAsignacion)
);

-- Agregar Curso
DELIMITER $$
CREATE PROCEDURE agregar_curso (IN des VARCHAR(50))
BEGIN
	INSERT INTO Curso(descripcion) VALUE(des);
END $$
DELIMITER ;

-- Agregar grupo
DELIMITER $$
CREATE PROCEDURE agregar_grupo (IN des VARCHAR(50))
BEGIN
	IF NOT EXISTS(SELECT * from grupo where descripcion = des) THEN
	INSERT INTO Grupo(descripcion) VALUE(des);
    END IF ;
END $$
DELIMITER ;

-- Agregar asignacion
DELIMITER $$
CREATE PROCEDURE agregar_asignacion (IN idG INT, IN idC INT)
BEGIN
	INSERT INTO Asignacion(idGrupo, idCurso) VALUE(idG, idC);
END $$
DELIMITER ;

-- FILTRO DE GRUPO POR CURSO
DELIMITER $$
CREATE PROCEDURE filtro_curso (IN des VARCHAR(30))
BEGIN
	SELECT grupo.descripcion, curso.descripcion 
    from asignacion 
    INNER JOIN grupo ON Asignacion.idGrupo = grupo.idGrupo
    inner join curso ON asignacion.idCurso = curso.idCurso
	WHERE curso.descripcion = des;
END $$
DELIMITER ;

-- FILTRO DE CURSO POR GRUPO
DELIMITER $$
CREATE PROCEDURE buscar_cursos (IN des VARCHAR(30))
BEGIN
	SELECT grupo.descripcion, curso.descripcion 
    from asignacion
    INNER JOIN grupo ON Asignacion.idGrupo = grupo.idGrupo
    inner join curso ON asignacion.idCurso = curso.idCurso
    WHERE grupo.descripcion = des;
END $$
DELIMITER ;

-- BUSCAR CURSO 
DELIMITER $$
CREATE PROCEDURE buscar_curso (IN idC INT)
BEGIN
	SELECT * FROM curso where idCurso = idC;
END $$
DELIMITER ;

-- BUSCAR Grupo 
DELIMITER $$
CREATE PROCEDURE buscar_grupo (IN idG INT)
BEGIN
	SELECT * FROM grupo where idGrupo = idG;
END $$
DELIMITER ;