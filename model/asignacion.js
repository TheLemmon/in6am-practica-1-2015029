var db = require('./database')

var asignacion = {
    selectAll: function(callback){
        if(db){
            db.query('SELECT * FROM Asignacion', function(err, rows){
                if(err){
                    throw err
                } else {
                    callback(null, rows)
                }
            })
        }
    },

    insert: function(data, callback){
        if(db){
            var query = 'CALL agregar_Asignacion(?)'
            db.query(query, data, function(err, rows){
                if(err){
                    throw err
                } else {
                    callback(null, {"insertId": rows.insertId})
                }
            })
        }
    }
}

module.exports = asignacion