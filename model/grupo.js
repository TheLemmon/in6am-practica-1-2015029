var db = require('./database')

var grupo = {
    selectAll: function(callback){
        if(db){
            db.query('SELECT * FROM grupo', function(err, rows){
                if(err){
                    throw err
                } else {
                    callback(null, rows)
                }
            })
        }
    },

    select : function(idgrupo, callback){
        if(db){
            var query = 'CALL buscar_cursos(?)'
            db.query(query, idgrupo, function(err, rows){
                if(err){
                    throw err
                } else {
                    callback(null, rows)
                }
            })
        }
    },

    insert: function(data, callback){
        if(db){
            var query = 'CALL agregar_grupo(?)'
            db.query(query, data.descripcion, function(err, rows){
                if(err){
                    throw err
                } else {
                    callback(null, rows)
                }
            })
        }
    }
}

module.exports = grupo