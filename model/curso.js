var db = require('./database')

var curso = {
    selectAll: function(callback){
        if(db){
            db.query('SELECT * FROM Curso', function(err, rows){
                if(err) {
                    throw err
                } else {
                    callback(null, rows)
                }
            })
        }
    },

    select : function(idCurso, callback){
        if(db){
            var query = 'CALL filtar_curso(?)'
            db.query(query, idCurso, function(err, rows){
                if(err){
                    throw err
                } else {
                    callback(null, rows)
                }
            })
        }
    },

    insert: function(data, callback){
        if(db){
            var query = 'CALL agregar_curso(?)'
            db.query(query, data, function(err, rows){
                if(err){
                    throw err
                } else {
                    callback(null, {"insertId": rows.insertId})
                }
            })
        }
    }
}

module.exports = curso