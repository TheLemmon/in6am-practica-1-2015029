var express = require('express')
var curso = require('../model/curso')
var router = express.Router()

router.get('/api/curso/', function(req, res){
    curso.selectAll(function(err, rows){
        if(typeof(rows) !== undefined){
            res.json(rows)
        } else {
            res.json({'Mensaje' : 'Error'})
        }
    })
})

router.post('/api/curso/', function(req, res){
    var data = {
        descripcion: req.body.descripcion
    }
    curso.insert(data, function(err, rows){
        if(typeof(rows) !== undefined){
            res.json(rows)
        } else {
            res.json({'Mensaje' : 'Error'})
        }
    })
})

router.get('/api/curso/:idcurso', function(req, res){
    var idcurso = req.params.idcurso
    curso.select(idcurso, function(err, rows){
        if(typeof(rows) !== undefined){
            res.json(rows)
        } else {
            res.json({'Mensaje' : 'Error'})
        }
    })
})

module.exports = router