var express = require('express')
var asignacion = require('../model/asignacion')
var router = express.Router()

router.get('/api/asignacion/', function(req, res){
    asignacion.selectAll(function(err, rows){
        if(typeof(rows) !== undefined){
            res.json(rows)
        } else {
            res.json({'Mensaje' : 'Error'})
        }
    })
})

router.post('/api/asignacion/', function(req, res){
    var data = {
        descripcion: req.body.descripcion
    }
    asignacion.insert(data, function(err, rows){
        if(typeof(rows) !== undefined){
            res.json(rows)
        } else {
            res.json({'Mensaje' : 'Error'})
        }
    })
})

module.exports = router