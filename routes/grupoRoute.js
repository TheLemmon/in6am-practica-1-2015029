var express = require('express')
var grupo = require('../model/grupo')
var router = express.Router()

router.get('/api/grupo/', function(req, res){
    grupo.selectAll(function(err, rows){
        if(typeof(rows) !== undefined){
            res.json(rows)
        } else {
            res.json({'Mensaje' : 'Error'})
        }
    })
})

router.post('/api/grupo/', function(req, res){
    var data = {
        descripcion: req.body.descripcion
    }
    grupo.insert(data, function(err, rows){
        if(typeof(rows) !== undefined){
            res.json(rows)
        } else {
            res.json({'Mensaje' : 'Error'})
        }
    })
})

router.get('/api/grupo/:idGrupo', function(req, res){
    var idGrupo = req.params.idGrupo
    grupo.select(idGrupo, function(err, rows){
        if(typeof(rows) !== undefined){
            res.json(rows)
        } else {
            res.json({'Mensaje' : 'Error'})
        }
    })
})

module.exports = router